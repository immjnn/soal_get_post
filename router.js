const express = require('express')

const router = express.Router();

const authenticate = (req, res, next) => {
  if (req.headers.user_id && req.headers.scope) {
    if (req.headers.user_id == 'ifabula' && req.headers.scope == 'user') {
      next();
    } else {
      return res.status(401).send({
        responseCode: 401, 
        responseMessage: "UNAUTHORIZED"
      })
    }
  } else {
    res.status(401).send({
      responseCode: 401, 
      responseMessage: "UNAUTHORIZED"
    })
  }
};

router.get('/user', authenticate, (req, res) => {
  res.send({
    name: 'Imam Jinani'
  })
})

router.post('/posts', authenticate, (req, res) => {
  res.send({
    title: 'Hi!',
    description: 'Nasi goreng'
  })
})

module.exports = router
